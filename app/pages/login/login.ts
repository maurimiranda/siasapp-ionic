import {Component} from '@angular/core';
import {NavController, Loading} from 'ionic-angular';
import {TranslateService, TranslatePipe} from 'ng2-translate/ng2-translate';
import {SessionService} from '../../providers/session-service/session-service'

@Component({
  templateUrl: 'build/pages/login/login.html',
  pipes: [TranslatePipe]
})
export class LoginPage {
  constructor(private nav: NavController, private sessionService: SessionService,
    private translate: TranslateService) {

  }

  login(loginData) {
    this.nav.present(Loading.create({
      content: this.translate.instant('loading.login'),
      dismissOnPageChange: true
    }));
    this.sessionService.login(loginData).then(() => {
      this.nav.pop();
    })
  }
}
