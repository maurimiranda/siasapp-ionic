import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {TranslatePipe} from "ng2-translate/ng2-translate";
import {Session} from '../../models/session';
import {SessionService} from '../../providers/session-service/session-service'

@Component({
  templateUrl: 'build/pages/home/home.html',
  pipes: [TranslatePipe]
})
export class HomePage {
  session: Session;

  constructor(private nav: NavController, private sessionService: SessionService) {
  }

  ionViewWillEnter() {
    this.session = this.sessionService.get();
  }
}
