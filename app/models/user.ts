export class User {
  uid: number;
  username: string;
  name: string;
  email: string;
  language: string;
}
