import {User} from './user';

export class Session {
  sessid: string;
  sessionName: string;
  token: string;
  user: User;

  getCookieString() {
    return this.sessionName + '=' + this.sessid;
  }
}
