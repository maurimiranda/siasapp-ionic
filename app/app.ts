import {Component, ViewChild} from '@angular/core';
import {ionicBootstrap, Platform, MenuController, Nav, Loading} from 'ionic-angular';
import {StatusBar} from 'ionic-native';
import {provide} from '@angular/core';
import {Http, HTTP_PROVIDERS} from '@angular/http';
import {TranslateService, TranslatePipe, TranslateLoader, TranslateStaticLoader} from 'ng2-translate/ng2-translate';

import {SessionService} from './providers/session-service/session-service'
import {Session} from './models/session';

import {HomePage} from './pages/home/home';
import {LoginPage} from './pages/login/login';

@Component({
  templateUrl: 'build/app.html',
  pipes: [TranslatePipe],
  providers: [SessionService]
})
export class SiasApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;
  loginPage: any = LoginPage;

  constructor(private platform: Platform, private translate: TranslateService,
    private menu: MenuController, private sessionService: SessionService) {
    platform.ready().then(() => {
      StatusBar.styleDefault();
    });

    this.initializeTranslateServiceConfig();
  }

  initializeTranslateServiceConfig() {
    var userLang = navigator.language.split('-')[0]; // use navigator lang if available
    userLang = /(en|es)/gi.test(userLang) ? userLang : 'en';
    this.translate.setDefaultLang('en');
    this.translate.use(userLang);
  }

  openPage(page) {
    this.menu.close();
    this.nav.push(page);
  }

  logout() {
    this.menu.close();
    this.presentLoading();
    this.sessionService.logout().then(() => {
      this.nav.setRoot(HomePage);
    });
  }

  presentLoading() {
    this.nav.present(Loading.create({
      content: this.translate.instant('loading.logout'),
      dismissOnPageChange: true
    }));
  }
}

ionicBootstrap(SiasApp, [[provide(TranslateLoader, {
  useFactory: (http: Http) => new TranslateStaticLoader(http, 'assets/i18n', '.json'),
  deps: [Http]
}), TranslateService]], {});
