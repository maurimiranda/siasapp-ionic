import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions, Response} from '@angular/http';
import 'rxjs/add/operator/map';

import {User} from '../../models/user';
import {Session} from '../../models/session';

@Injectable()
export class SessionService {
  session: Session;

  constructor(private http: Http) {
  }

  get() {
    if (!this.session) {
      this.session = JSON.parse(localStorage.getItem('session'));
    }
    return this.session;
  }

  login(loginData) {
    return new Promise(resolve => {
      var data = {
        'name': loginData.username,
        'pass': loginData.password
      };
      this.http.post('api/user/login',
      // this.http.post('http://dev.siasar.org/rest/user/login',
        JSON.stringify(data), new RequestOptions(
          {
            headers: new Headers({
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            })
          }
        ))
        .map(this.parseData)
        .subscribe(data => {
          this.session = data;
          document.cookie = this.session.getCookieString();
          localStorage.setItem('session', JSON.stringify(data));
          resolve(data);
        });
    });
  }

  logout() {
    return new Promise(resolve => {
      this.http.post('api/user/logout', null, new RequestOptions(
      // this.http.post('http://dev.siasar.org/rest/user/logout', null, new RequestOptions(
          {
            headers: new Headers({
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'X-CSRF-Token': this.session.token
            })
          }
        ))
        .subscribe(data => {
          this.session = null;
          localStorage.removeItem('session');
          resolve();
        });
    });
  }

  private parseData(res: Response) {
    let response = res.json();
    let session: Session = new Session();
    let user: User = new User();

    user.uid = response.user.uid;
    user.username = response.user.name;
    user.email = response.user.mail;
    user.language = response.user.language;
    user.name = response.user.field_nombre_completo ? response.user.field_nombre_completo[user.language][0].safe_value : user.username;

    session.sessid = response.sessid;
    session.sessionName = response.session_name;
    session.token = response.token;
    session.user = user;

    return session;
  }
}
